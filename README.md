# Ensuring water security in Tanzania: Predicting the working state of Tanzanian community water pumps

Capstone project concluding  the data science certificate program.

* Merges multiple datasets including water pump data from the Tanzanian Ministry of Water and precipitation predictions from NOAA.
* Data cleansing, feature imputation, risk encoding and exploratory data analysis of 35 categorical, numerical and binary features (M01).
* Feature selection using three approaches: LASSO regression, Random Forest feature importances, and stepwise feature selection (M02).
* Machine learning and baseline modeling using three  approaches: Logistic Regression, SVM, Random Forest (M02).
* Model optimization through grid search, hyperparameter tuning and adaptive boosting (M03).

Quickstart:

* Download Python 3 notebooks: LoganDowning-M01-Capstone-Proposal.ipynb, LoganDowning-M02-Capstone-Checkin.ipynb, LoganDowning-M03-Capstone-Project.ipynb
* Download data sources located in the data/ folder
* Open and run the three notebooks using Jupyter Notebook

Or view [screen grabs](screenshots/) of all three notebooks.

I created a [presentation](LoganDowning-Capstone-Datasci420-Water-Pumps.pdf) of the capstone that reviews the entire project and includes a summary of the approach, analysis, modeling and conclusions.  (LoganDowning-Capstone-Datasci420-Water-Pumps.pdf)

___

Plot the distribution of datasets, including locations of Tanzanian water pumps (red) and positions of weather stations (blue).

![Map of Tanzania with positions of water pumps and weather stations](img/tanzania.png)

Explore the variation of precipitation across the entire country.

![Distribution of precipitation values across the country, shown in a square grid by latitude/longitude](img/precip.png)

Use LASSO feature reduction to explore reducing features to a smaller  set.

![Bar plot showing relative strengths of a subset of features](img/lasso.png)

Use grid search to optimize performance by selecting the best hyperparameter values.

![Box plots showing the results of a large grid search over a range of hyperparameters](img/gridsearch.png)

Evaluate the performance of the machine learning model through a receiver operating characteristic (ROC) curve using cross-validated fits.

![Receiver operating curve](img/roc.png)
